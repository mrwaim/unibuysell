package com.klsandbox.unibuysell;

/**
 * Created by ibrar on 5/19/15.
 */
public class UserInfo {
    public int id;
    public String name;

    public UserInfo(int id, String name)
    {
        this.id = id;
        this.name = name;
    }
}
