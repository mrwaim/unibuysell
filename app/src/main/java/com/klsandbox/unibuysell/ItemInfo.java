package com.klsandbox.unibuysell;

import java.util.HashMap;

/**
 * Created by ibrar on 5/19/15.
 */
public class ItemInfo {
    public int id;
    public String name;
    public String price;
    public UserInfo user;


    public ItemInfo(int id, UserInfo user, String name, String price)
    {
        this.id = id;
        this.user = user;
        this.name = name;
        this.price = price;
    }

    public HashMap<String,String> toHashMap()
    {
        HashMap<String, String> hash = new HashMap<String, String>();
        hash.put("id", Integer.toString(id));
        hash.put("user", user.name);
        hash.put("name", this.name);
        hash.put("price", this.price);

        return hash;
    }
}
