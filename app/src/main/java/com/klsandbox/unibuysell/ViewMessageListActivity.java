package com.klsandbox.unibuysell;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.SimpleAdapter;


public class ViewMessageListActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_message_list);

        SimpleAdapter adapter = new SimpleAdapter(
                this,
                DataStore.DB.getMessages(),
                R.layout.list_element_message,
                new String[] { "from", "to", "item", "message" },
                new int[] { R.id.from_text, R.id.to_text, R.id.item_text, R.id.message_text} );

        ListView listView = (ListView)findViewById(R.id.list_messages_view);
        listView.setAdapter(adapter);

    }

}
