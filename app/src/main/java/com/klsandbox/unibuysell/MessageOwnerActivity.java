package com.klsandbox.unibuysell;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class MessageOwnerActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_owner);

        Intent myIntent = getIntent(); // gets the previously created intent
        int item_id = myIntent.getIntExtra("item_id", -1);

        final ItemInfo item = DataStore.DB.getItemById(item_id);
        final Context context = this;

        Button button = (Button)findViewById(R.id.button_message_send);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String message = ((EditText)findViewById(R.id.message_text)).getText().toString();

                DataStore.DB.sendMessage(item.user, item, message);

                String say = ("New message - count:" + DataStore.DB.messages.size());

                Toast.makeText(context, say, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
