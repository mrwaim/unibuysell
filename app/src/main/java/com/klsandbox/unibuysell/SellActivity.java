package com.klsandbox.unibuysell;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


public class SellActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sell);

        final Context context = this;

        Button b = (Button)findViewById(R.id.button_sell);
        b.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                String name = ((TextView)findViewById(R.id.item_name)).getText().toString();
                String price = ((TextView)findViewById(R.id.item_price)).getText().toString();
                ItemInfo item = DataStore.DB.addItem(name, price);

                Toast.makeText(context, "Name: " + name + " price:" + price + " db items:" + DataStore.DB.items.size(), Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(context, ViewItemActivity.class);
                intent.putExtra("item_id", item.id);
                startActivity(intent);
            }
        });

    }
}
