package com.klsandbox.unibuysell;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


public class ViewItemActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_item);

        final Context context = this;

        Intent myIntent = getIntent(); // gets the previously created intent
        int item_id = myIntent.getIntExtra("item_id", -1);

        final ItemInfo item = DataStore.DB.getItemById(item_id);

        TextView nameText = (TextView)findViewById(R.id.item_name);
        nameText.setText(item.name);

        TextView priceText = (TextView)findViewById(R.id.item_price);
        priceText.setText(item.price);

        Button messageOwnerButton = (Button)findViewById(R.id.button_message_owner);
        messageOwnerButton.setOnClickListener(new View.OnClickListener() {
                                                  @Override
                                                  public void onClick(View v) {
                                                      Intent intent = new Intent(context, MessageOwnerActivity.class);
                                                      intent.putExtra("item_id", item.id);
                                                      startActivity(intent);
                                                  }
                                              }

        );
    }
}
