package com.klsandbox.unibuysell;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;


public class ViewListActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_list);

        SimpleAdapter adapter = new SimpleAdapter(
                this,
                DataStore.DB.getItems(),
                android.R.layout.simple_list_item_2,
                new String[] { "name", "price" },
                new int[] { android.R.id.text1, android.R.id.text2 } );

        ListView listView = (ListView)findViewById(R.id.list_items_view);
        listView.setAdapter(adapter);

        final Context context = this;

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                HashMap<String, String> map = (HashMap<String, String>)parent.getItemAtPosition(position);

                int item_id = Integer.parseInt(map.get("id"));
                Toast.makeText(context, "Hello " + map.get("name"), Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(context, ViewItemActivity.class);
                intent.putExtra("item_id", item_id);
                startActivity(intent);
            }
        });

    }
}
