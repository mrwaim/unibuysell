package com.klsandbox.unibuysell;

import android.os.Message;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by ibrar on 5/19/15.
 */
public class DataStore {

    public static DataStore DB = new DataStore();

    int seq = 0;
    public List<UserInfo> users = new ArrayList<UserInfo>();
    public List<ItemInfo> items = new ArrayList<ItemInfo>();
    public List<MessageInfo> messages = new ArrayList<MessageInfo>();

    public DataStore()
    {
        this.seed();
    }

    public UserInfo loggedInUser;

    public UserInfo addUser(String name)
    {
        UserInfo user = new UserInfo(seq++, name);
        users.add(user);
        return user;
    }

    public ItemInfo addItem(String name, String price)
    {
        ItemInfo item = new ItemInfo(seq++, this.loggedInUser, name, price);
        items.add(item);
        return item;
    }

    public MessageInfo sendMessage(UserInfo to, ItemInfo item, String message)
    {
        MessageInfo messageInfo = new MessageInfo(seq++, loggedInUser, to, item, message);
        messages.add(messageInfo);
        return messageInfo;
    }

    public void seed()
    {
        UserInfo user1 = this.addUser("ibrahim");
        UserInfo user2 = this.addUser("waim");

        this.loggedInUser = user2;

        ItemInfo item1 = this.addItem("red bike with two wheels", "100");
        ItemInfo item2 = this.addItem("ipod nano", "200");

        MessageInfo message = this.sendMessage(user2, item1, "Can I buy the bike?");

    }

    public ItemInfo getItemById(int item_id) {
        for (ItemInfo item : this.items)
        {
            if (item.id == item_id)
            {
                return item;
            }
        }

        return null;
    }

    public ArrayList<HashMap<String, String>> getItems()
    {
        ArrayList<HashMap<String, String>> dataList = new ArrayList<HashMap<String, String>>();
        for (ItemInfo info : this.items)
        {
            dataList.add(info.toHashMap());
        }

        return dataList;
    }

    public ArrayList<HashMap<String, String>> getMessages()
    {
        ArrayList<HashMap<String, String>> dataList = new ArrayList<HashMap<String, String>>();
        for (MessageInfo message : this.messages)
        {
            dataList.add(message.toHashMap());
        }

        return dataList;
    }
}
