package com.klsandbox.unibuysell;

import java.util.Date;
import java.util.HashMap;

/**
 * Created by ibrar on 5/19/15.
 */
public class MessageInfo {
    public int id;
    public UserInfo from;
    public UserInfo to;
    public ItemInfo item;
    public String message;
    public Date timestamp;

    public MessageInfo(int id, UserInfo from, UserInfo to, ItemInfo item, String message)
    {
        this.id = id;
        this.from = from;
        this.to = to;
        this.item = item;
        this.message = message;
        this.timestamp = new Date();
    }

    public HashMap<String,String> toHashMap()
    {
        HashMap<String, String> hash = new HashMap<String, String>();
        hash.put("id", Integer.toString(id));
        hash.put("from", from.name);
        hash.put("to", to.name);
        hash.put("message", this.message);
        hash.put("item", this.item.name);

        return hash;
    }
}
