package com.klsandbox.unibuysell;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;


public class BaseActivity extends Activity {

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_sell) {
            Toast.makeText(this, "Sell", Toast.LENGTH_SHORT).show();

            Intent intent = new Intent(this, SellActivity.class);
            startActivity(intent);

            return true;
        }
        else if (id == R.id.action_view_list) {
            Toast.makeText(this, "View List", Toast.LENGTH_SHORT).show();

            Intent intent = new Intent(this, ViewListActivity.class);
            startActivity(intent);

            return true;
        }
        else if (id == R.id.action_view_message) {
            Toast.makeText(this, "View Message", Toast.LENGTH_SHORT).show();

            Intent intent = new Intent(this, ViewMessageListActivity.class);
            startActivity(intent);

            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
}
